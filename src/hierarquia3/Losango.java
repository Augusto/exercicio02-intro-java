package hierarquia3;

public class Losango extends FiguraGeometrica{
	public void calculoArea(float base, float altura){
		float area = (base * altura) / 2;
		setArea(area);
	}
}
