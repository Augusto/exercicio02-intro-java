package hierarquia3;

public class Retangulo extends FiguraGeometrica{
	public void calculoArea(float base, float altura){
		float area = (base * altura);
		setArea(area);
	}
}
