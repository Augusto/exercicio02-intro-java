package hierarquia3;

public class Triangulo extends FiguraGeometrica{
	private int angulo1;
	private int angulo2;
	private int angulo3;
	
	public void calculoArea(float base, float altura){
		float area = (base * altura) / 2;
		setArea(area);
	}

	public int getAngulo1() {
		return angulo1;
	}

	public void setAngulo1(int angulo1) {
		this.angulo1 = angulo1;
	}

	public int getAngulo2() {
		return angulo2;
	}

	public void setAngulo2(int angulo2) {
		this.angulo2 = angulo2;
	}

	public int getAngulo3() {
		return angulo3;
	}

	public void setAngulo3(int angulo3) {
		this.angulo3 = angulo3;
	}

}
