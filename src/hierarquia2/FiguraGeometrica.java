package hierarquia2;

public class FiguraGeometrica {
	private int quantidadeLados;
	private int quantidadeArestas;
	private int quantidadeFaces;
	private float area;
	
	public int getQuantidadeLados() {
		return quantidadeLados;
	}
	public void setQuantidadeLados(int quantidadeLados) {
		this.quantidadeLados = quantidadeLados;
	}
	public int getQuantidadeArestas() {
		return quantidadeArestas;
	}
	public void setQuantidadeArestas(int quantidadeArestas) {
		this.quantidadeArestas = quantidadeArestas;
	}
	public int getQuantidadeFaces() {
		return quantidadeFaces;
	}
	public void setQuantidadeFaces(int quantidadeFaces) {
		this.quantidadeFaces = quantidadeFaces;
	}
	public float getArea() {
		return area;
	}
	public void setArea(float area) {
		this.area = area;
	}
}
